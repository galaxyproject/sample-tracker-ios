# Galaxy Specimen Tracking #

Welcome to the Galaxy specimen tracking system.

### Other parts of Galaxy Specimen Tracking###
 * The specimen tracking galaxy repo is available here: https://bitbucket.org/martenson/galaxy-central-sample-tracking
 * The Android application (mirror of this one) is available here: https://bitbucket.org/galaxyproject/sample-tracker-android

### Version
 * latest version 0.4

### What is this repository for? ###

This repo contains sources for iOS application that can communicate with the Galaxy linked above. Its main purpose is to help manage tubes with lab specimens through the usage of barcode scanner.

### How do I get set up? ###

Unlike in the Android version of this app the barcode scanner engine is included in the sources.

You have to run the specimen-tracking version of galaxy that the app wil communicate with. There has to be at least one project created in the Galaxy DB and at least one user.

You have to enter user's API key, project id and Galaxy's URL in the mobile app.


### Who do I talk to? ###

Write me at marten@bx.psu.edu